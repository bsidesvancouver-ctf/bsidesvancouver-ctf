This repo contains all the configuration and code planning needed for the BsidesVancouver-CTF 

Aspects covered
*  CTFd configuration - export config via .zip
*  host/vm scenarios notes and configs
*  host/vm images -- Currently not hosted on Gitlab due to space issues 
*  Challanges, keys, flags and info. 
*  Misc 
